﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad_2
{
        class RandomGenerator
        {

            private static RandomGenerator instance;
            private Random generator;
            private RandomGenerator()
            {
                this.generator = new Random();
            }
            public static RandomGenerator GetInstance()
            {
                if (instance == null)
                {
                    instance = new RandomGenerator();
                }
                return instance;
            }


        public double[][] NextMatrix(int rows, int collumns)
        {
            double[][] matrix = new double[rows][];
            for (int i = 0; i < rows; i++)
            {
                matrix[i] = new double[collumns];
            }
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    matrix[i][j] = this.generator.NextDouble();
                }
            }
            return matrix;
        }

    }

}
