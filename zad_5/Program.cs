﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad_5
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager Builder = new NotificationManager();
            Builder.SetAuthor("Dino");
            Builder.SetColor(ConsoleColor.Yellow);
            Builder.SetLevel(Category.INFO);
            Builder.SetText("Hello, my name is Dino.");
            Builder.SetTime(DateTime.Now);
            Builder.SetTitle("HELLO");
            ConsoleNotification msg = Builder.Build();
            Builder.Display(msg);
        }
    }
}
