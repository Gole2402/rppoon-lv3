﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset d1 = new Dataset("test.txt");
            Dataset d2 = (Dataset)d1.Clone();

            d1.Print();
            d2.Change("change");
            d2.Print();
            d1.ClearData();

            d1.Print();
            d2.Print();
        }
    }
}
