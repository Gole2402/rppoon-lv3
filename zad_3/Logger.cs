﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad_3
{
    class Logger
    {
        private static Logger instance;
        private string filepath;
        public Logger(string filepath)
        {
            this.filepath = filepath;
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger("C:\\Users\\Dino\\source\\repos\\RPPOON_LV3\\zad_3\\log.txt");
            }
            return instance;
        }
        public string FilePath
        {
            get { return filepath; }
            set { filepath = value; }
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filepath, true))
            {
                writer.WriteLine(message);
            }

        }
    }
}
